import React from 'react';
import './techSkills.css';
import html from '../../assets/html.png';
import css from '../../assets/cp.png';
import jname from '../../assets/javascript.png';
import node from  '../../assets/node.png';
import rimage from '../../assets/react.png';
import pyimage from '../../assets/python.png';

const TechSkills = () => {
    return (
              <div>
                <h2 className='headingSkills'>Competencies</h2>
            <section className="flexContainer">
           
            <div className="mainSkillsBox">
                <div className="skillImageBox">
                    <img className="SkillImage" src={html} alt="HTML"/>
                </div>
            </div>
            <div className="mainSkillsBox">
                <div className="skillImageBox">
                    <img className="SkillImage" src={css} alt="CSS"/>
                </div>
            </div>
            <div className="mainSkillsBox">
                <div className="skillImageBox">
                    <img className="SkillImage" src={jname} alt="JavaScript"/>
                </div>
            </div>
            <div className="mainSkillsBox">
                <div className="skillImageBox">
                    <img className="SkillImage" src={node} alt="Node.js"/>
                </div>
            </div>
            <div className="mainSkillsBox">
                <div className="skillImageBox">
                    <img className="SkillImage" src={rimage} alt="React"/>
                </div>
            </div>
            <div className="mainSkillsBox">
                <div className="skillImageBox">
                    <img className="SkillImage" src={pyimage} alt="Python"/>
                </div>
            </div>
        </section>
        </div>
    );
}

export default TechSkills;
