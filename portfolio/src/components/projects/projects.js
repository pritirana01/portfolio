import React from 'react'
import './project.css'
import ui from '../../assets/UI.png'
const Projects=()=>{
    return(
      <div>
        <h1 className='projectHeading'>Projects</h1>

<section id="projects">
        
        <div className="projectBars">
               <div className="projectbar">
                <img src={ui} className="projectBarImg"></img>
                <div className="projectBartext">
                    <h2 className='projectName'>Library Mangement</h2>
                    <p className='projectPara'>The frontend of the project was built using HTML, CSS and Java Script and the Backend was
built using Node js and MongoDB. It has various endpoints tailored for managers. These
endpoints include functionalities such as sign-up, login, adding member, updating existing
member records, adding book, updating book record, issue and return a book.</p>
                </div>
                
                
            </div>
            <div className="projectbar">
                <img src={ui} className="projectBarImg"></img>
                <div className="projectBartext">
                    <h2 className='projectNameNews'>News Application</h2>
                    <p className='projectParaNews'>During the mini-hackathon organized by HyperVerge Academy, I actively contributed to the development of a news application. Employing a stack comprising HTML, CSS, and JavaScript, I spearheaded the project to integrate two distinct endpoints from a news API. This enabled us to dynamically fetch and render data, enriching the application's functionality and user experience.




</p>
                </div>
                
                
            </div>
            <div className="projectbar">
                <img src={ui} className="projectBarImg"></img>
                <div className="projectBartext">
                    <h2 className='projectNameBus'>Bus Ticketing</h2>
                    <p className='projectoaraBus'>Bus ticketing application is a backend project developed using Node.js and MongoDB,
having functionality for both admin and user. The admin login grants authorized access to
functionalities such as adding new bus, view booked ticket. The user can access features
such as view available bus, book ticket, cancel ticket.</p>
                </div>
                
                
            </div>
            </div>
         
         
    </section>
      </div>
       
    
    )
}
export default Projects