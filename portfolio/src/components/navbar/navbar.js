import React from "react";
import './navbar.css'
import logo from '../../assets/Plogo.jpg'
import contactLogo from '../../assets/message.png'
import {Link} from 'react-scroll'
function Navbar(){
    return(
    <div>
        <nav className="navbar">
        <img src={logo} alt="" className="logo"></img>
        <div className="desktopMenu">
            <Link className="desktopmenuItem">Home</Link>
            <Link className="desktopmenuItem">About</Link>
            <Link className="desktopmenuItem">Skills</Link>
            <Link className="desktopmenuItem">Projects</Link>
        </div>
        <button className="desktopbutton">
            <img src={contactLogo} alt="" className="desktopButtonimg"></img>contact me

        </button>
        </nav>
     
    </div>
    )
}
export default Navbar