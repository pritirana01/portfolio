import React from "react";
import './about.css'
import ui from '../../assets/UI.png'
const Skills=()=>{
    return(
        <section id="skills">
            <span className="skillsTitle">What I Do</span>
            <span className="skillsDesc">I'm a full stack developer, which means I work on all parts of websites or apps. I'm good at making things look nice and work smoothly using HTML, CSS, and JavaScript. I also handle the behind-the-scenes stuff with technologies like Node.js and Express.js. I make sure everything works well together!</span>
            <div className="skillBars">
                <div className="skillbar">
                    <img src={ui} className="skillBarImg"></img>
                    <div className="skillBartext">
                        <h2>API crafting</h2>
                        <p>Designing APIs to smoothly link apps, simplifying interactions and boosting functionality.</p>
                    </div>
                    
                    
                </div>
                <div className="skillbar">
                    <img src={ui} className="skillBarImg"></img>
                    <div className="skillBartext">
                        <h2>UI Design</h2>
                        <p>Creating intuitive interfaces for delightful user experiences across various platforms.</p>
                    </div>
                    
                    
                </div>
                <div className="skillbar">
                    <img src={ui} className="skillBarImg"></img>
                    <div className="skillBartext">
                        <h2>Web Design</h2>
                        <p>Designing websites with user-friendly layouts, captivating visuals, and seamless navigation.</p>
                    </div>
                    
                    
                </div>
            </div>
        </section>
    )
}
export default Skills