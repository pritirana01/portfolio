import React from 'react'
import './intro.css'
import myImg  from '../../assets/me.png'
import {Link} from 'react-scroll'
const Intro=()=>{
    return(
        <section id='introSection'>
            <div className='introSection'>
                <span className='spanHello'>Hello,</span>
                <span className='spamiam'>I,m <span className='introName'>Preeti Rana</span><br></br>Website Designer</span>
                <p className='introPara'>I am a skilled web designer with experience in creating <br></br> visually appealing and user friendly websites.</p>
           
            </div>
            <img alt='myimg' src={myImg} className='myImg'></img>
        </section>
    )
}
export default Intro